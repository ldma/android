# LDMA iOs APP

Android APP For the Final Project of MOBILE STARTUP ENGINEERING BOOTCAMP IV

Devoloped with Java

## TODO List

- Activity Detail
- Employee Detail
- Saving Activity with detail (map + invited employees)
- Activities Map
- Services Map
- Profile
- Services list
- Activity Infinite Scroll
- Activity Server Filtering
- Services Infinite Scroll
- Services Server Filtering
- Chat

## TEAM

- [Eric Risco][1]
- [Begoña Hormaechea][2]
- [Alberto Galera][3]
- [Paco Cardenal][4]
- [Eugenio Barquín][5]

[1]: https://github.com/eriscoand
[2]: https://github.com/begohorma
[3]: https://github.com/albertgs
[4]: https://github.com/pacocardenal
[5]: https://github.com/ebarquin

[10]:	https://s3.eu-west-2.amazonaws.com/com.ldma.frontend/index.html
