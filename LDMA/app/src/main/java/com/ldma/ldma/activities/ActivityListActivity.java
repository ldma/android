package com.ldma.ldma.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.list.ListInteractor;
import com.ldma.domain.interactors.list.ListInteractorCompletion;
import com.ldma.domain.interactors.list.activites.ActivitiesListInteractorImpl;
import com.ldma.domain.managers.network.managers.ActivitiesManagerImplementation;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.domain.models.Activity.Activity;
import com.ldma.ldma.R;
import com.ldma.ldma.fragments.ActivitiesFragment;
import com.ldma.ldma.views.OnElementClick;

import java.util.List;

/**
 * Created by erisco on 18/10/2017.
 */

public class ActivityListActivity extends AppCompatActivity {
    private final String TAG ="ActivityListActivitiy";
    private ProgressDialog waitDialog;

    ActivitiesFragment activitiesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list);

        showWaitDialog(getString(R.string.loading_activities));

        activitiesFragment = (ActivitiesFragment) getSupportFragmentManager().findFragmentById(R.id.activity_activities_list__fragment_activities);

        NetworkManager manager = new ActivitiesManagerImplementation(this);
        ListInteractor interactor = new ActivitiesListInteractorImpl(getString(R.string.LIST_ACTIVITY_URL) + "&company_id=1", manager);

        interactor.execute(
                new ListInteractorCompletion<List<Activity>>() {
                    @Override
                    public void completion(@NonNull List<Activity> entity) {
                        Activities activities = Activities.from(entity);
                        configActivitiesFragment(activities);
                        closeWaitDialog();
                    }
                },
                new InteractorErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        Log.d("JSON", errorDescription);
                        closeWaitDialog();
                    }
                }
        );

    }

    private void configActivitiesFragment(final Activities activities) {
        activitiesFragment.setActivities(activities);
        activitiesFragment.setOnElementClickListener(new OnElementClick<Activity>() {
            @Override
            public void clickedOn(@NonNull Activity activity, int position) {
                //Navigator.navigateFromActivityListActivityToActivityDetailActivity(ActivityListActivity.this, activity, position);
            }
        });
    }

    private void showWaitDialog(String message) {
        closeWaitDialog();
        waitDialog = new ProgressDialog(this);
        waitDialog.setTitle(message);
        waitDialog.show();
    }

    private void closeWaitDialog() {
        try{
            waitDialog.dismiss();
        }
        catch(Exception e){
            System.out.println("Exception: " + e);
        }
    }

}
