package com.ldma.ldma.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ldma.domain.models.Activity.Activity;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.ldma.R;
import com.ldma.ldma.views.OnElementClick;
import com.ldma.ldma.views.RowViewHolder;

public class ActivitiesAdapter extends RecyclerView.Adapter<RowViewHolder> {
    private Activities activities;
    private LayoutInflater inflater;
    private OnElementClick<Activity> listener;

    public ActivitiesAdapter(final Activities activities, final Context context) {
        this.activities = activities;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_element, parent, false);

        RowViewHolder viewHolder = new RowViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RowViewHolder row, final int position) {
        final Activity activity = this.activities.get(position);

        String map = "http://maps.googleapis.com/maps/api/staticmap?center=<<LAT>>,<<LNG>>&zoom=17&size=150x150&scale=2&markers=%7Ccolor:0x9C7B14%7C<<LAT>>,<<LNG>>";

        map.replace("<<LAT>>", activity.getLatitude());
        map.replace("<<LNG>>", activity.getLongitude());

        row.set(activity.getName(), map);

        row.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.clickedOn(activity, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (this.activities != null) {
            return (int) this.activities.size();
        }
        return 0;
    }

    public void setOnClickListener(OnElementClick<Activity> listener) {
        this.listener = listener;
    }
}
