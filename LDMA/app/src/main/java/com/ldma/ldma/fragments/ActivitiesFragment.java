package com.ldma.ldma.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ldma.domain.models.Activity.Activity;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.ldma.R;
import com.ldma.ldma.adapters.ActivitiesAdapter;
import com.ldma.ldma.views.OnElementClick;

public class ActivitiesFragment extends Fragment {

    private OnElementClick<Activity> listener;

    private RecyclerView activitiesRecyclerView;
    private ActivitiesAdapter adapter;
    private Activities activities;

    public ActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activities, container, false);

        activitiesRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_activities__recycler_view);
        activitiesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    public void setActivities(Activities activities) {
        this.activities = activities;

        adapter = new ActivitiesAdapter(activities, getActivity());
        activitiesRecyclerView.setAdapter(adapter);

        adapter.setOnClickListener(new OnElementClick<Activity>() {
            @Override
            public void clickedOn(@NonNull Activity activity, int position) {
                if (listener != null) {
                    ActivitiesFragment.this.listener.clickedOn(activity, position);
                }
            }
        });
    }

    public void setOnElementClickListener(OnElementClick<Activity> listener) {
        this.listener = listener;
    }

}
