package com.ldma.ldma.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.Employee.Employees;
import com.ldma.ldma.R;
import com.ldma.ldma.views.OnElementClick;
import com.ldma.ldma.views.RowViewHolder;

public class EmployeesAdapter extends RecyclerView.Adapter<RowViewHolder> {
    private Employees employees;
    private LayoutInflater inflater;
    private OnElementClick<Employee> listener;

    public EmployeesAdapter(final Employees employees, final Context context) {
        this.employees = employees;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_element, parent, false);

        RowViewHolder viewHolder = new RowViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RowViewHolder row, final int position) {
        final Employee employee = this.employees.get(position);
        row.set(employee.getName() + " " + employee.getSurnames(), "https://s3.eu-west-2.amazonaws.com/com.ldma.profile/" + employee.getAvatar_url());

        row.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.clickedOn(employee, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (this.employees != null) {
            return (int) this.employees.size();
        }
        return 0;
    }

    public void setOnClickListener(OnElementClick<Employee> listener) {
        this.listener = listener;
    }
}
