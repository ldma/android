package com.ldma.ldma.navigators;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.ldma.ldma.activities.ActivityListActivity;
import com.ldma.ldma.activities.EmployeeListActivity;
import com.ldma.ldma.activities.HomeActivity;
import com.ldma.ldma.activities.MainActivity;

/**
 * Created by erisco on 18/10/2017.
 */

public class Navigator {

    public static Intent navigateFromMainToHomeActivity(@NonNull final MainActivity mainActivity){
        assert(mainActivity != null);

        final Intent i = new Intent(mainActivity, HomeActivity.class);
        mainActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromHomeToEmployeeListActivity(@NonNull final HomeActivity homeActivity) {
        assert(homeActivity != null);

        final Intent i = new Intent(homeActivity, EmployeeListActivity.class);
        homeActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromHomeToActivityListActivity(@NonNull final HomeActivity homeActivity) {
        assert(homeActivity != null);

        final Intent i = new Intent(homeActivity, ActivityListActivity.class);
        homeActivity.startActivity(i);

        return i;
    }

}
