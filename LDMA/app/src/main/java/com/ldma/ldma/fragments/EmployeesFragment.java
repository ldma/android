package com.ldma.ldma.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.Employee.Employees;
import com.ldma.ldma.R;
import com.ldma.ldma.adapters.EmployeesAdapter;
import com.ldma.ldma.views.OnElementClick;

public class EmployeesFragment extends Fragment {

    private OnElementClick<Employee> listener;

    private RecyclerView employeesRecyclerView;
    private EmployeesAdapter adapter;
    private Employees employees;

    public EmployeesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employees, container, false);

        employeesRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_employees__recycler_view);
        employeesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;

        adapter = new EmployeesAdapter(employees, getActivity());
        employeesRecyclerView.setAdapter(adapter);

        adapter.setOnClickListener(new OnElementClick<Employee>() {
            @Override
            public void clickedOn(@NonNull Employee employee, int position) {
                if (listener != null) {
                    EmployeesFragment.this.listener.clickedOn(employee, position);
                }
            }
        });
    }

    public void setOnElementClickListener(OnElementClick<Employee> listener) {
        this.listener = listener;
    }

}
