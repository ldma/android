package com.ldma.ldma.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;
import com.ldma.ldma.R;
import com.ldma.ldma.util.CognitoHelper;

import java.util.Map;

public class UserActivity extends AppCompatActivity {
    private final String TAG="UserActivity";

    private AlertDialog userDialog;
    private ProgressDialog waitDialog;
    //private ListView attributesList;

    // Cognito user objects
    private CognitoUser user;
//    private CognitoUserSession session;
//    private CognitoUserDetails details;

    // User details
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        init();
    }
    public void logOut(View view){
        logOutUser();
    }
    // Sign out user
    private void logOutUser() {
        user.signOut();
        exit();
    }
    // Initialize this activity
    private void init() {
        // Get the user name
        username = CognitoHelper.getCurrUser();
        user = CognitoHelper.getPool().getUser(username);
        getDetails();
    }

    // Get user details from CIP service
    private void getDetails() {
        CognitoHelper.getPool().getUser(username).getDetailsInBackground(detailsHandler);
    }

    // Show user attributes from CIP service
    private void showAttributes() {
        // Ejemplo de cómo se accede a los datos del usuario
        Map attrs = CognitoHelper.getUserDetails().getAttributes().getAttributes();

        TextView email = (TextView) findViewById(R.id.activity_user__textViewUserEmail);
        email.setText(attrs.get("email").toString());

        TextView profile = (TextView) findViewById(R.id.activity_user__textViewUserProfile);
        profile.setText(attrs.get("profile").toString());

    }

    GetDetailsHandler detailsHandler = new GetDetailsHandler() {
        @Override
        public void onSuccess(CognitoUserDetails cognitoUserDetails) {
            closeWaitDialog();
            // Store details in the AppHandler
            CognitoHelper.setUserDetails(cognitoUserDetails);
            showAttributes();

        }

        @Override
        public void onFailure(Exception exception) {
            closeWaitDialog();
            showDialogMessage("Could not fetch user details!", CognitoHelper.formatException(exception), true);
        }
    };
//
    private void closeWaitDialog() {
        try {
            waitDialog.dismiss();
        }
        catch (Exception e) {
            System.out.println("Exception" + e);
        }
    }

    private void showDialogMessage(String title, String body, final boolean exit) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(body).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                    if(exit) {
                        exit();
                    }
                } catch (Exception e) {
                    // Log failure
                    Log.e(TAG,"Dialog dismiss failed");
                    if(exit) {
                        exit();
                    }
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }

    private void exit () {
        Intent intent = new Intent();
        if(username == null)
            username = "";
        intent.putExtra("name",username);
        setResult(RESULT_OK, intent);
        finish();
    }
}
