package com.ldma.ldma.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler;
import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.delete.DeleteInteractor;
import com.ldma.domain.interactors.delete.DeleteInteractorCompletion;
import com.ldma.domain.interactors.delete.activities.ActivitiesDeleteInteractorImpl;
import com.ldma.domain.interactors.save.SaveInteractor;
import com.ldma.domain.interactors.save.SaveInteractorCompletion;
import com.ldma.domain.interactors.save.activites.ActivitiesSaveInteractorImpl;
import com.ldma.domain.managers.network.managers.ActivitiesManagerImplementation;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.models.Activity.Activity;
import com.ldma.ldma.R;
import com.ldma.ldma.navigators.Navigator;
import com.ldma.ldma.util.CognitoHelper;

import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    private final String TAG ="HomeActivity";

    @BindView(R.id.activity_home__activity_list_button)
    Button activitiesButton;

    @BindView(R.id.activity_home__employee_list_button)
    Button employeesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        employeesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigator.navigateFromHomeToEmployeeListActivity(HomeActivity.this);
            }
        });

        activitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigator.navigateFromHomeToActivityListActivity(HomeActivity.this);
            }
        });

    }

}


