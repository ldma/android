package com.ldma.ldma;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;

import com.squareup.picasso.Picasso;


public class LDMAApp extends MultiDexApplication {

    public static final String APP_NAME = LDMAApp.class.getCanonicalName();

    @Override
    public void onCreate() {
        super.onCreate();

        // init app

        //Picasso.with(getApplicationContext()).setLoggingEnabled(true);
        //Picasso.with(getApplicationContext()).setIndicatorsEnabled(true);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        // low memory: dump something
    }
}

