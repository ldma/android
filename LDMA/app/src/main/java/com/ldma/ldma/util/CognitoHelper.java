package com.ldma.ldma.util;


import android.content.Context;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient;

public class CognitoHelper {

    private static CognitoUserPool userPool;
    private static  String user;

    //TODO - poner en archivo de constantes o similar
    // cognito user pool values
    private static final String userPoolId = "eu-west-2_apnz3dE0N";
    private static final String clientId = "c43n1bvg2cf5hivines08pq14";
    private static final String clientSecret = "10ij4upr3srbj9vbcmh29pr9j8pnk4bggc1b3lf95fb8qljjqc2u";
    private static final Regions cognitoRegion = Regions.EU_WEST_2;

    // User details from the service
    private static CognitoUserSession currSession;
    private static CognitoUserDetails userDetails;


    public static void init(Context context) {
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        AmazonCognitoIdentityProvider cipClient = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), clientConfiguration);
        cipClient.setRegion(Region.getRegion(cognitoRegion));
        userPool = new CognitoUserPool(context, userPoolId, clientId, clientSecret, cipClient);

    }
    public static String getCurrUser() {
        return user;
    }
    public static void setUser(String newUser) {
        user = newUser;
    }

    public static CognitoUserPool getPool() {
        return userPool;
    }

    public static void setCurrSession(CognitoUserSession session){
        currSession = session;
    }


    public static void setUserDetails(CognitoUserDetails details) {
        userDetails = details;

    }
    public static  CognitoUserDetails getUserDetails() {
        return userDetails;
    }

    public static String formatException(Exception exception) {
        String formattedString = "Internal Error";
        Log.e("LDMA App Error",exception.toString());
        Log.getStackTraceString(exception);

        String temp = exception.getMessage();

        if(temp != null && temp.length() > 0) {
            formattedString = temp.split("\\(")[0];
            if(temp != null && temp.length() > 0) {
                return formattedString;
            }
        }

        return  formattedString;
    }
}
