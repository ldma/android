package com.ldma.ldma.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.list.ListInteractor;
import com.ldma.domain.interactors.list.ListInteractorCompletion;
import com.ldma.domain.interactors.list.employees.EmployeesListInteractorImpl;
import com.ldma.domain.managers.network.managers.EmployeesManagerImplementation;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.Employee.Employees;
import com.ldma.ldma.R;
import com.ldma.ldma.fragments.EmployeesFragment;
import com.ldma.ldma.navigators.Navigator;
import com.ldma.ldma.views.OnElementClick;

import java.util.List;

import butterknife.BindView;

/**
 * Created by erisco on 18/10/2017.
 */

public class EmployeeListActivity extends AppCompatActivity {
    private final String TAG ="EmployeeListActivitiy";
    private ProgressDialog waitDialog;

    EmployeesFragment employeesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);

        showWaitDialog(getString(R.string.loading_employees));

        employeesFragment = (EmployeesFragment) getSupportFragmentManager().findFragmentById(R.id.activity_employees_list__fragment_employees);

        NetworkManager manager = new EmployeesManagerImplementation(this);
        ListInteractor interactor = new EmployeesListInteractorImpl(getString(R.string.LIST_EMPLOYEE_URL) + "?company_id=1", manager);

        interactor.execute(
                new ListInteractorCompletion<List<Employee>>() {
                    @Override
                    public void completion(@NonNull List<Employee> entity) {
                        Employees employees = Employees.from(entity);
                        configEmployeesFragment(employees);
                        closeWaitDialog();
                    }
                },
                new InteractorErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        Log.d("JSON", errorDescription);
                        closeWaitDialog();
                    }
                }
        );

    }

    private void configEmployeesFragment(final Employees employees) {
        employeesFragment.setEmployees(employees);
        employeesFragment.setOnElementClickListener(new OnElementClick<Employee>() {
            @Override
            public void clickedOn(@NonNull Employee employee, int position) {
                //Navigator.navigateFromActivityListActivityToActivityDetailActivity(ActivityListActivity.this, activity, position);
            }
        });
    }

    private void showWaitDialog(String message) {
        closeWaitDialog();
        waitDialog = new ProgressDialog(this);
        waitDialog.setTitle(message);
        waitDialog.show();
    }

    private void closeWaitDialog() {
        try{
            waitDialog.dismiss();
        }
        catch(Exception e){
            System.out.println("Exception: " + e);
        }
    }

}
