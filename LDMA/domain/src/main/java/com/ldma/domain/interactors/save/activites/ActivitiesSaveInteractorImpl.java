package com.ldma.domain.interactors.save.activites;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.list.ListInteractor;
import com.ldma.domain.interactors.list.ListInteractorCompletion;
import com.ldma.domain.interactors.save.SaveInteractor;
import com.ldma.domain.interactors.save.SaveInteractorCompletion;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.managers.Interfaces.ListManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ManagerErrorCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.managers.network.managers.Interfaces.SaveManagerCompletion;
import com.ldma.domain.managers.network.mappers.ActivitiesMapper;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.domain.models.Activity.Activity;

import java.util.List;

public class ActivitiesSaveInteractorImpl implements SaveInteractor<Activity> {

    private NetworkManager networkManager;
    private String url;
    private Activity activity;

    public ActivitiesSaveInteractorImpl(@NonNull final String url, @NonNull final Activity activity, @NonNull final NetworkManager networkManager) {
        this.networkManager = networkManager;
        this.url = url;
        this.activity = activity;
    }

    @Override
    public void execute(@NonNull final SaveInteractorCompletion<Activity> completion, @Nullable final InteractorErrorCompletion onError) {
        if (this.networkManager == null) {
            if (onError == null) {
                throw new IllegalStateException("Network manager can't be null");
            } else {
                onError.onError("");
            }
        }

        this.networkManager.saveToServer(
                this.url,
                activity.toJSON(),
                new SaveManagerCompletion<JsonActivity>() {
                    @Override
                    public void completion(@NonNull JsonActivity jsonEntity) {
                        if(completion != null){
                            Activity activity = new ActivitiesMapper().mapOne(jsonEntity);
                            completion.completion(activity);
                        }
                    }
                },
                new ManagerErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        if (onError != null) {
                            onError.onError(errorDescription);
                        }
                    }
                }

        );
    }
}
