package com.ldma.domain.managers.network.mappers.Interfaces;

import java.util.List;

public interface Mapper<T,M,S> {
    S mapOne(final T entity);
    M map(final List<T> entities);
}
