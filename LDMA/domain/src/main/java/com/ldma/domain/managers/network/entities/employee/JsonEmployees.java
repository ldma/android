package com.ldma.domain.managers.network.entities.employee;

import com.google.gson.annotations.SerializedName;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;

import java.util.List;

/**
 * Created by erisco on 19/10/2017.
 */

public class JsonEmployees {
    @SerializedName("") private List<JsonEmployee> result;

    public List<JsonEmployee> getResult() {
        return result;
    }
}
