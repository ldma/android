package com.ldma.domain.managers.network.managers.Interfaces;

import android.support.annotation.NonNull;

public interface DeleteManagerCompletion<T> {
    void completion(@NonNull final T jsonEntity);
}
