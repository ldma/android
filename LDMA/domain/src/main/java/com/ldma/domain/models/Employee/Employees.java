package com.ldma.domain.models.Employee;

import android.support.annotation.NonNull;

import com.ldma.domain.models.Queryable;
import com.ldma.domain.models.Updatable;
import com.ldma.domain.models.Iterable;

import java.util.LinkedList;
import java.util.List;

public class Employees implements Iterable<Employee>, Updatable<Employee>, Queryable<Employee> {
    private List<Employee> employees;

    public static Employees from(@NonNull final List<Employee> employeesList) {
        final Employees employees = new Employees();

        for (final Employee employee : employeesList) {
            employees.add(employee);
        }

        return employees;
    }

    public Employees() {
    }

    // lazy getter
    private List<Employee> getEmployees() {
        if (employees == null) {
            employees =  new LinkedList<>();
        }
        return employees;
    }

    @Override
    public void add(Employee element) {
        getEmployees().add(element);
    }

    @Override
    public void delete(Employee element) {
        getEmployees().remove(element);
    }

    @Override
    public void update(Employee element, long index) {
        getEmployees().set((int)index, element);
    }

    @Override
    public long size() {
        return employees.size();
    }

    @Override
    public Employee get(long index) {
        return getEmployees().get((int)index);
    }

    @Override
    public List<Employee> allElements() {
        List<Employee> listCopy = new LinkedList<>();
        for (Employee employee : getEmployees()) {
            listCopy.add(employee);
        }

        return listCopy;
    }

    @Override
    public List<Employee> query(String query) {
        List<Employee> returnEmployees = new LinkedList<>();

        for (Employee employee : this.employees) {

            String email = employee.getEmail();
            email = email.replace(" ", "");
            email = email.toLowerCase();

            int i = email.indexOf(query);
            if (i >= 0) {
                returnEmployees.add(employee);
            }
        }
        return returnEmployees;
    }
}
