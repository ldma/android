package com.ldma.domain.managers.network.managers.Interfaces;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.save.SaveInteractorCompletion;

import org.json.JSONObject;


public interface NetworkManager {
    void saveToServer(@NonNull final String url,
                      @NonNull final JSONObject json,
                      @NonNull final SaveManagerCompletion completion,
                      @NonNull final ManagerErrorCompletion errorCompletion);
    void deleteToServer(@NonNull final String url,
                      @NonNull final DeleteManagerCompletion completion,
                      @NonNull final ManagerErrorCompletion errorCompletion);
    void getListFromServer(@NonNull final String url,
                           @NonNull final ListManagerCompletion completion,
                           @Nullable final ManagerErrorCompletion errorCompletion);
}
