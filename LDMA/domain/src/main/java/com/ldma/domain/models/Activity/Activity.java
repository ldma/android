package com.ldma.domain.models.Activity;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.JSONAble;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

public class Activity implements Serializable, JSONAble {
    private String id;
    private String company_id;
    //private Company company;  //TODO: Add company
    private String service_id;
    //private Service service;  //TODO: Add service
    private String name;
    private String thumbnail_url;
    private String cover_url;
    private String description;
    private String owner_id;
    private Date init_date;
    private Date end_date;
    private Boolean isPrivate;
    private Boolean isDeleted;
    private Employee owner;
    private String latitude;
    private String longitude;

    public Activity() {
    }


    public static Activity of(String id, String name){
        Activity activity = new Activity();

        activity.setId(id);
        activity.setName(name);

        return activity;
    }

    public String getId() {
        return id;
    }

    public Activity setId(String id) {
        this.id = id;
        return this;
    }

    public String getCompany_id() {
        return company_id;
    }

    public Activity setCompany_id(String company_id) {
        this.company_id = company_id;
        return this;
    }

    public String getService_id() {
        return service_id;
    }

    public Activity setService_id(String service_id) {
        this.service_id = service_id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Activity setName(String name) {
        this.name = name;
        return this;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public Activity setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
        return this;
    }

    public String getCover_url() {
        return cover_url;
    }

    public Activity setCover_url(String cover_url) {
        this.cover_url = cover_url;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Activity setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public Activity setOwner_id(String owner_id) {
        this.owner_id = owner_id;
        return this;
    }

    public Employee getOwner() {
        return owner;
    }

    public Activity setOwner(Employee owner) {
        this.owner = owner;
        return this;
    }

    public Date getInit_date() {
        return init_date;
    }

    public Activity setInit_date(Date init_date) {
        this.init_date = init_date;
        return this;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public Activity setEnd_date(Date end_date) {
        this.end_date = end_date;
        return this;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public Activity setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
        return this;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public Activity setDeleted(Boolean aDeleted) {
        isDeleted = aDeleted;
        return this;
    }

    public String getLatitude() {
        return latitude;
    }

    public Activity setLatitude(String aLatitude) {
        latitude = aLatitude;
        return this;
    }

    public String getLongitude() {
        return longitude;
    }

    public Activity setLongitude(String aLongitude) {
        longitude = aLongitude;
        return this;
    }

    @Override
    public JSONObject toJSON() {
        try{
            Gson gson = new Gson();
            String jsonString = gson.toJson(this);
            return new JSONObject(jsonString);
        }catch (Exception ex){
            return new JSONObject();
        }
    }
}
