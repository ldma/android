package com.ldma.domain.interactors;

public interface InteractorErrorCompletion {
    void onError(String errorDescription);
}
