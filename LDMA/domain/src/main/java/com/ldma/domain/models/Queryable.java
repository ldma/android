package com.ldma.domain.models;

import java.util.List;

public interface Queryable<T> {
    List<T> query(String query);
}
