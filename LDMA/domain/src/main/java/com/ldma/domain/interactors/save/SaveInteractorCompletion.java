package com.ldma.domain.interactors.save;

import android.support.annotation.NonNull;

public interface SaveInteractorCompletion<T> {
    void completion(@NonNull final T entity);
}
