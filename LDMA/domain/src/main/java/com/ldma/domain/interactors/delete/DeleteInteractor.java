package com.ldma.domain.interactors.delete;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.InteractorErrorCompletion;

public interface DeleteInteractor<T> {
    void execute(@NonNull final DeleteInteractorCompletion<T> completion, @Nullable final InteractorErrorCompletion onError);
}
