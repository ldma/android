package com.ldma.domain.managers.network.mappers;

import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.mappers.Interfaces.Mapper;
import com.ldma.domain.models.Activity.Activities;
import com.ldma.domain.models.Activity.Activity;

import java.util.List;

public class ActivitiesMapper implements Mapper<JsonActivity, Activities, Activity> {

    public ActivitiesMapper(){}

    @Override
    public Activity mapOne(JsonActivity entity) {

        Activity activity = Activity.of(entity.getId(), entity.getName())
                .setCompany_id(entity.getCompany_id())
                .setService_id((entity.getService_id()))
                .setName(entity.getName())
                .setThumbnail_url(entity.getThumbnail_url())
                .setCover_url(entity.getCover_url())
                .setDescription(entity.getDescription())
                .setOwner_id(entity.getOwner_id())
                .setPrivate(entity.getIsPrivate())
                .setDeleted(entity.getIsDeleted())
                .setLatitude(entity.getLatitude())
                .setLongitude(entity.getLongitude());

        return activity;
    }

    @Override
    public Activities map(List<JsonActivity> entities) {
        Activities activities = new Activities();

        for (JsonActivity json : entities) {

            Activity activity = this.mapOne(json);

            activities.add(activity);
        }

        return activities;
    }
}