package com.ldma.domain.interactors.delete.activities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.delete.DeleteInteractor;
import com.ldma.domain.interactors.delete.DeleteInteractorCompletion;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.managers.Interfaces.DeleteManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ManagerErrorCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.managers.network.mappers.ActivitiesMapper;
import com.ldma.domain.models.Activity.Activity;

public class ActivitiesDeleteInteractorImpl implements DeleteInteractor<Activity> {

    private NetworkManager networkManager;
    private String url;
    private Activity activity;

    public ActivitiesDeleteInteractorImpl(@NonNull final String url, @NonNull final Activity activity, @NonNull final NetworkManager networkManager) {
        this.networkManager = networkManager;
        this.url = url;
        this.activity = activity;
    }

    @Override
    public void execute(@NonNull final DeleteInteractorCompletion<Activity> completion, @Nullable final InteractorErrorCompletion onError) {
        if (this.networkManager == null) {
            if (onError == null) {
                throw new IllegalStateException("Network manager can't be null");
            } else {
                onError.onError("");
            }
        }

        this.networkManager.deleteToServer(
                this.url + "/" + this.activity.getId(),
                new DeleteManagerCompletion<JsonActivity>() {
                    @Override
                    public void completion(@NonNull JsonActivity jsonEntity) {
                        if(completion != null){
                            Activity activity = new ActivitiesMapper().mapOne(jsonEntity);
                            completion.completion(activity);
                        }
                    }
                },
                new ManagerErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        if (onError != null) {
                            onError.onError(errorDescription);
                        }
                    }
                }
        );
    }
}
