package com.ldma.domain.managers.network.managers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.jsonparser.JsonActivityParser;
import com.ldma.domain.managers.network.managers.Interfaces.DeleteManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ListManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ManagerErrorCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.managers.network.managers.Interfaces.SaveManagerCompletion;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivitiesManagerImplementation implements NetworkManager {

    WeakReference<Context> weakContext;

    public ActivitiesManagerImplementation(Context context) {
        weakContext = new WeakReference<Context>(context);
    }


    @Override
    public void saveToServer(@NonNull final String url, @NonNull final JSONObject json, @NonNull final SaveManagerCompletion completion, @NonNull final ManagerErrorCompletion errorCompletion) {

        RequestQueue queue = Volley.newRequestQueue(weakContext.get());

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                url,
                json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON", response.toString());

                        JsonActivity entity = new JsonActivityParser().parseOne(response.toString());

                        if(completion != null){
                                completion.completion(entity);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (errorCompletion != null) {
                            errorCompletion.onError(new VolleyErrorResponse().onErrorResponse(error));
                        }
                    }
                }
        );

        queue.add(request);

    }

    @Override
    public void deleteToServer(@NonNull final String url, @NonNull final DeleteManagerCompletion completion, @NonNull final ManagerErrorCompletion errorCompletion) {
        RequestQueue queue = Volley.newRequestQueue(weakContext.get());

        final String contentType = "application/json; charset=utf-8";
        final Map<String, String> headers = new HashMap<>();

        StringRequest request = new StringRequest(
                Request.Method.DELETE,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("JSON", response.toString());

                        JsonActivity entity = new JsonActivityParser().parseOne(response.toString());

                        if(completion != null){
                            completion.completion(entity);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (errorCompletion != null) {
                            errorCompletion.onError(new VolleyErrorResponse().onErrorResponse(error));
                        }
                    }
                }
        ){
            @Override
            public String getBodyContentType()
            {
                return contentType;
            }
            @Override
            public Map<String, String> getHeaders() {
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        queue.add(request);
    }

    @Override
    public void getListFromServer(@NonNull final String url, @NonNull final ListManagerCompletion completion, @Nullable final ManagerErrorCompletion errorCompletion) {
        RequestQueue queue = Volley.newRequestQueue(weakContext.get());

        StringRequest request = new StringRequest(
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("JSON", response);

                        JsonActivityParser parser = new JsonActivityParser();
                        List<JsonActivity> entities = parser.parse(response);

                        if (completion != null) {
                            completion.completion(entities);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("JSON", error.toString());
                        if (errorCompletion != null) {
                            errorCompletion.onError(error.getMessage());
                        }
                    }
                }
        );
        queue.add(request);
    }
}
