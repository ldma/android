package com.ldma.domain.models;

import java.util.List;

public interface Iterable<T> {
    long size();
    T get(long index);
    List<T> allElements();
}
