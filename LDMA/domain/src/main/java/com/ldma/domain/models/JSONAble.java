package com.ldma.domain.models;


import android.support.annotation.NonNull;

import org.json.JSONObject;

public interface JSONAble {
    JSONObject toJSON();
}
