package com.ldma.domain.models.Employee;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.ldma.domain.models.JSONAble;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

public class Employee implements Serializable, JSONAble {
    private String id;
    private String email;
    private String company_id;
    private String name;
    private String surnames;
    private String avatar_url;
    private Boolean isDeleted;

    public Employee(){}

    public static Employee of(String id, String email){
        Employee employee = new Employee();

        employee.setId(id);
        employee.setEmail(email);

        return employee;
    }


    public String getId() {
        return id;
    }

    public Employee setId(@NonNull final String id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCompany_id() {
        return company_id;
    }

    public Employee setCompany_id(String company_id) {
        this.company_id = company_id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurnames() {
        return surnames;
    }

    public Employee setSurnames(String surnames) {
        this.surnames = surnames;
        return this;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public Employee setAvatar_url(String avatar_url){
        this.avatar_url = avatar_url;
        return this;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public Employee setIsDeleted(Boolean isDeleted){
        this.isDeleted = isDeleted;
        return this;
    }

    @Override
    public JSONObject toJSON() {
        try{
            Gson gson = new Gson();
            String jsonString = gson.toJson(this);
            return new JSONObject(jsonString);
        }catch (Exception ex){
            return new JSONObject();
        }
    }
}
