package com.ldma.domain.managers.network.entities.employee;

import com.google.gson.annotations.SerializedName;

public class JsonEmployee {

    @SerializedName("id") private String id;
    @SerializedName("name") private String name;
    @SerializedName("surnames") private String surnames;
    @SerializedName("email") private String email;
    @SerializedName("avatar_url") private String avatar_url;
    @SerializedName("company_id") private String company_id;
    @SerializedName("isDeleted") private Boolean isDeleted;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurnames() {
        return surnames;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getCompany_id() {
        return company_id;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }
}
