package com.ldma.domain.managers.network.managers;


import android.util.Log;

import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;

public class VolleyErrorResponse {

    public String onErrorResponse(VolleyError error) {

        String body = "";

        if(error.networkResponse.data!=null) {
            try {
                body = new String(error.networkResponse.data,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        Log.d("JSON", body);
        return body;

    }
}
