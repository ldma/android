package com.ldma.domain.interactors.save;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.InteractorErrorCompletion;

public interface SaveInteractor<T> {
    void execute(@NonNull final SaveInteractorCompletion<T> completion, @Nullable final InteractorErrorCompletion onError);
}
