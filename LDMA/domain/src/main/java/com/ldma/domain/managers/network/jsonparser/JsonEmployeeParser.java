package com.ldma.domain.managers.network.jsonparser;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ldma.domain.managers.network.entities.activity.JsonActivities;
import com.ldma.domain.managers.network.entities.employee.JsonEmployee;
import com.ldma.domain.managers.network.entities.employee.JsonEmployees;
import com.ldma.domain.managers.network.jsonparser.Interfaces.JsonParser;

import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

public class JsonEmployeeParser implements JsonParser<JsonEmployee> {

    public JsonEmployee parseOne(@NonNull String response) {
        if (response == null) {
            return null;
        }

        JsonEmployee jsonEntity = null;

        try {
            Gson gson = new GsonBuilder().create();

            Reader reader = new StringReader(response);
            jsonEntity = gson.fromJson(reader, JsonEmployee.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonEntity;
    }

    public List<JsonEmployee> parse(@NonNull final String response) {
        if (response == null) {
            return null;
        }

        List<JsonEmployee> jsonEntities = null;

        try {
            Gson gson = new GsonBuilder().create();

            Reader reader = new StringReader(response);
            JsonEmployee[] jsonResponseEntity = gson.fromJson(reader, JsonEmployee[].class);

            jsonEntities = Arrays.asList(jsonResponseEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonEntities;
    }

}
