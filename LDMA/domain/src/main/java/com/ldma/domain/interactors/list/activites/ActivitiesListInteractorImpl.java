package com.ldma.domain.interactors.list.activites;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.list.ListInteractor;
import com.ldma.domain.interactors.list.ListInteractorCompletion;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.managers.Interfaces.ListManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ManagerErrorCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.managers.network.mappers.ActivitiesMapper;
import com.ldma.domain.models.Activity.Activities;

import java.util.List;

public class ActivitiesListInteractorImpl implements ListInteractor {

    private NetworkManager networkManager;
    private String url;

    public ActivitiesListInteractorImpl(@NonNull final String url, @NonNull final NetworkManager networkManager) {
        this.networkManager = networkManager;
        this.url = url;
    }

    @Override
    public void execute(@NonNull final ListInteractorCompletion completion, @Nullable final InteractorErrorCompletion onError) {
        if (this.networkManager == null) {
            if (onError == null) {
                throw new IllegalStateException("Network manager can't be null");
            } else {
                onError.onError("");
            }
        }

        this.networkManager.getListFromServer(
                this.url,
                new ListManagerCompletion<JsonActivity>() {
                        @Override
                        public void completion(@NonNull List<JsonActivity> jsonActivities) {
                            if (completion != null) {
                                Activities activities = new ActivitiesMapper().map(jsonActivities);
                                completion.completion(activities);
                            }
                        }
                    },
                new ManagerErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        if (onError != null) {
                            onError.onError(errorDescription);
                        }
                    }
        });
    }
}
