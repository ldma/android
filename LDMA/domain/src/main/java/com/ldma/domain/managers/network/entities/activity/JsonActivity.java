package com.ldma.domain.managers.network.entities.activity;

import com.google.gson.annotations.SerializedName;

public class JsonActivity {

    @SerializedName("id") private String id;
    @SerializedName("company_id") private String company_id;
    //@SerializedName("company") private Company company;  //TODO: Add company
    @SerializedName("service_id") private String service_id;
    //@SerializedName("service") private Service service;  //TODO: Add service
    @SerializedName("name") private String name;
    @SerializedName("thumbnail_url") private String thumbnail_url;
    @SerializedName("cover_url") private String cover_url;
    @SerializedName("description") private String description;
    @SerializedName("owner_id") private String owner_id;
    @SerializedName("isPrivate") private Boolean isPrivate;
    @SerializedName("isDeleted") private Boolean isDeleted;
    //@SerializedName("owner") private JsonEmployee owner; //TODO: Add JsonEmployee
    @SerializedName("latitude") private String latitude;
    @SerializedName("longitude") private String longitude;



    public String getId() {
        return id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public String getService_id() {
        return service_id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public String getCover_url() {
        return cover_url;
    }

    public String getDescription() {
        return description;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

}
