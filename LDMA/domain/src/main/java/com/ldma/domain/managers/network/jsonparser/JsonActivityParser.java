package com.ldma.domain.managers.network.jsonparser;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ldma.domain.managers.network.entities.activity.JsonActivities;
import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.jsonparser.Interfaces.JsonParser;
import com.ldma.domain.models.Activity.Activity;

import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.List;

public class JsonActivityParser implements JsonParser<JsonActivity> {

    public JsonActivity parseOne(@NonNull String response) {
        if (response == null) {
            return null;
        }

        JsonActivity jsonEntity = null;

        try {
            Gson gson = new GsonBuilder().create();

            Reader reader = new StringReader(response);
            jsonEntity = gson.fromJson(reader, JsonActivity.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonEntity;
    }

    public List<JsonActivity> parse(@NonNull final String response) {
        if (response == null) {
            return null;
        }

        List<JsonActivity> jsonEntities = null;

        try {
            Gson gson = new GsonBuilder().create();

            Reader reader = new StringReader(response);
            JsonActivities jsonResponseEntity = gson.fromJson(reader, JsonActivities.class);

            jsonEntities = jsonResponseEntity.getResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonEntities;
    }

}
