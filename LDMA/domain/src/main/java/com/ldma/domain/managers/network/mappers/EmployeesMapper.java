package com.ldma.domain.managers.network.mappers;


import com.ldma.domain.managers.network.entities.activity.JsonActivity;
import com.ldma.domain.managers.network.entities.employee.JsonEmployee;
import com.ldma.domain.managers.network.mappers.Interfaces.Mapper;
import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.Employee.Employees;

import java.util.ArrayList;
import java.util.List;

public class EmployeesMapper implements Mapper<JsonEmployee, List<Employee>, Employee> {

    public EmployeesMapper(){}

    @Override
    public Employee mapOne(JsonEmployee entity) {
        Employee employee = Employee.of(entity.getId(),entity.getEmail())
                .setName(entity.getName())
                .setSurnames(entity.getSurnames())
                .setAvatar_url(entity.getAvatar_url())
                .setCompany_id(entity.getCompany_id())
                .setIsDeleted(entity.getIsDeleted());
        return employee;
    }

    @Override
    public List<Employee> map(List<JsonEmployee> entities) {
        List<Employee> employees = new ArrayList<Employee>();

        for (JsonEmployee json : entities) {

            Employee employee = this.mapOne(json);

            employees.add(employee);
        }

        return employees;
    }
}