package com.ldma.domain.managers.network.jsonparser.Interfaces;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.util.List;


public interface JsonParser<T> {
    T parseOne(@NonNull final String response);
    List<T> parse(@NonNull final String response);
}
