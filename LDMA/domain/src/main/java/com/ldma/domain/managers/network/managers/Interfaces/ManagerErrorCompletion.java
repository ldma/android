package com.ldma.domain.managers.network.managers.Interfaces;

public interface ManagerErrorCompletion {
    void onError(String errorDescription);
}
