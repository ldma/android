package com.ldma.domain.models.Activity;

import android.support.annotation.NonNull;

import com.ldma.domain.models.Queryable;
import com.ldma.domain.models.Updatable;
import com.ldma.domain.models.Iterable;

import java.util.LinkedList;
import java.util.List;

public class Activities implements Iterable<Activity>, Updatable<Activity>, Queryable<Activity> {
    private List<Activity> activities;

    public static Activities from(@NonNull final List<Activity> employeesList) {
        final Activities activities = new Activities();

        for (final Activity activity : employeesList) {
            activities.add(activity);
        }

        return activities;
    }

    public Activities() {
    }

    // lazy getter
    private List<Activity> getActivities() {
        if (activities == null) {
            activities =  new LinkedList<>();
        }
        return activities;
    }

    @Override
    public void add(Activity element) {
        getActivities().add(element);
    }

    @Override
    public void delete(Activity element) {
        getActivities().remove(element);
    }

    @Override
    public void update(Activity element, long index) {
        getActivities().set((int)index, element);
    }

    @Override
    public long size() {
        return activities.size();
    }

    @Override
    public Activity get(long index) {
        return getActivities().get((int)index);
    }

    @Override
    public List<Activity> allElements() {
        List<Activity> listCopy = new LinkedList<>();
        for (Activity activity : getActivities()) {
            listCopy.add(activity);
        }

        return listCopy;
    }

    @Override
    public List<Activity> query(String query) {
        List<Activity> returnActivities = new LinkedList<>();

        for (Activity activity : this.activities) {

            String name = activity.getName();
            name = name.replace(" ", "");
            name = name.toLowerCase();

            int i = name.indexOf(query);
            if (i >= 0) {
                returnActivities.add(activity);
            }
        }
        return returnActivities;
    }
}
