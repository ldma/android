package com.ldma.domain.managers.network.entities.activity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsonActivities {
    @SerializedName("") private List<JsonActivity> result;

    public List<JsonActivity> getResult() {
        return result;
    }
}
