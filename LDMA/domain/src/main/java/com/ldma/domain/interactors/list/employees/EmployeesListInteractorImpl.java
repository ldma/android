package com.ldma.domain.interactors.list.employees;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ldma.domain.interactors.InteractorErrorCompletion;
import com.ldma.domain.interactors.list.ListInteractor;
import com.ldma.domain.interactors.list.ListInteractorCompletion;
import com.ldma.domain.managers.network.entities.employee.JsonEmployee;
import com.ldma.domain.managers.network.entities.employee.JsonEmployees;
import com.ldma.domain.managers.network.managers.Interfaces.ListManagerCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.ManagerErrorCompletion;
import com.ldma.domain.managers.network.managers.Interfaces.NetworkManager;
import com.ldma.domain.managers.network.mappers.EmployeesMapper;
import com.ldma.domain.models.Employee.Employee;
import com.ldma.domain.models.Employee.Employees;

import java.util.List;

/**
 * Created by erisco on 19/10/2017.
 */

public class EmployeesListInteractorImpl implements ListInteractor {

    private NetworkManager networkManager;
    private String url;

    public EmployeesListInteractorImpl(@NonNull final String url, @NonNull final NetworkManager networkManager) {
        this.networkManager = networkManager;
        this.url = url;
    }

    @Override
    public void execute(@NonNull final ListInteractorCompletion completion, @Nullable final InteractorErrorCompletion onError) {
        if (this.networkManager == null) {
            if (onError == null) {
                throw new IllegalStateException("Network manager can't be null");
            } else {
                onError.onError("");
            }
        }

        this.networkManager.getListFromServer(
                this.url,
                new ListManagerCompletion<JsonEmployee>() {
                    @Override
                    public void completion(@NonNull List<JsonEmployee> jsonEmployees) {
                        if (completion != null) {
                            List<Employee> employees = new EmployeesMapper().map(jsonEmployees);
                            completion.completion(employees);
                        }
                    }
                },
                new ManagerErrorCompletion() {
                    @Override
                    public void onError(String errorDescription) {
                        if (onError != null) {
                            onError.onError(errorDescription);
                        }
                    }
                });
    }
}
