package com.ldma.domain.managers.network.managers.Interfaces;

import android.support.annotation.NonNull;

import java.util.List;

public interface SaveManagerCompletion<T> {
    void completion(@NonNull final T jsonEntity);
}
