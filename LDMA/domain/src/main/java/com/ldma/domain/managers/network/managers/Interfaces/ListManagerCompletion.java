package com.ldma.domain.managers.network.managers.Interfaces;

import android.support.annotation.NonNull;

import java.util.List;

public interface ListManagerCompletion<T> {
    void completion(@NonNull final List<T> jsonEntities);
}
