package com.ldma.domain.interactors.delete;

import android.support.annotation.NonNull;

public interface DeleteInteractorCompletion<T> {
    void completion(@NonNull final T entity);
}
